# MatuREST

Library to make REST Services development easy as fuck.

## Killer Features

- Type System instead of Magic Macroses
- Not a Framework
- It's written in Rust

## How it looks

### Out-of-the-box

```rust
Service::new(
	Route::at("/courses", Resource<Course>::new())
).build().start()?.await;
```

### Sligthly custom

```rust
Service::new(
    Route::at("/courses", Resource<Course>::custom().without_delete().build())
).build().start()?.await;
```

## How it works

Developers declare Resource with minimum amount of configuration (20%) to receive most of the 
functionality they need (80%) out-of-the-box, while the rest of the functionality or changes in
out-of-the-box functionality can be done via simple (yet additional up to 80%) declarations.
