use maturest::{
    Identifiable, Link,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct TestStruct {
    id: u32,
    link: Link<AnotherTestStruct>,
}

impl Identifiable for TestStruct {
    type Id = u32;

    fn identity() -> &'static str {
        "test"
    }

    fn id(&self) -> u32 {
        self.id
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct AnotherTestStruct {
    id: u32,
}

impl Identifiable for AnotherTestStruct {
    type Id = u32;

    fn identity() -> &'static str {
        "another_test"
    }

    fn id(&self) -> u32 {
        self.id
    }
}

#[test]
fn serialize_and_deserialize() {
    let structure = TestStruct {
        id: 0,
        link: Link::new(1),
    };
    assert_eq!(
        structure,
        serde_json::from_str(
            &serde_json::to_string(&structure).expect("Failed to serialize struct.")
        ).expect("Failed to deserialize struct.")
    );
}
